import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JFrame;

public class Window extends JFrame implements MouseListener, KeyListener {
	
	private ArrayList<Point> initialPoints;
	private ArrayList<Point> chaosPoints;
	private boolean playing;
	
	public Window() {
		
		super("Chaos_any");
		super.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		super.setLocationRelativeTo(null);
		super.setSize(660, 535);
		super.setLocationByPlatform(true);
		super.setVisible(true);
		super.addMouseListener(this);
		super.addKeyListener(this);
		initialPoints = new ArrayList<>();
		chaosPoints = new ArrayList<>();
		playing = false;

		
	}
	
	@Override
	public void paint(Graphics g) {
		g.setColor(Color.BLACK);
		for(Point p : initialPoints) {
			g.fillOval(p.getX(), p.getY(), 7, 7);
		}
		g.setColor(Color.GREEN);
		for(Point p : chaosPoints) {
			g.fillOval(p.getX(), p.getY(), 3, 3);
		}
		
		
	}
	

	@Override
	public void mousePressed(MouseEvent event) {
		
		if(!playing) {
			initialPoints.add(new Point(event.getX(), event.getY()));
			repaint();
		}
	}
	
	@Override
	public void keyPressed(KeyEvent event) {
		
		if(event.getKeyCode() == KeyEvent.VK_SPACE) {
			playGame();
		}
		
	}
	
	public void playGame() {
		
		//check if valid game possible
		if(initialPoints.size() < 1)
			return;
		
		playing = true;
		int x = initialPoints.get(0).getX();
		int y = initialPoints.get(0).getY();
		final int NUM_STEPS = 10000;
		Random r = new Random();
		
		for(int i = 0; i < NUM_STEPS; i++) {
			int travel = r.nextInt(initialPoints.size());
			int newX = (initialPoints.get(travel).getX() + x) / 2;
			int newY = (initialPoints.get(travel).getY() + y) / 2;
			
			chaosPoints.add(new Point(newX, newY));
			repaint();
			
			x = newX;
			y = newY;
			
			
		}
		
		
		
		
	}
	
	
	
	
	//-----------------------GARBAGE-----------------------\\
	

	@Override
	public void mouseClicked(MouseEvent arg0) {}

	@Override
	public void mouseEntered(MouseEvent arg0) {}

	@Override
	public void mouseExited(MouseEvent arg0) {}

	@Override
	public void mouseReleased(MouseEvent arg0) {}

	@Override
	public void keyReleased(KeyEvent arg0) {}

	@Override
	public void keyTyped(KeyEvent arg0) {}


}
